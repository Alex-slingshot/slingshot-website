/**
 * PERSONALIZED COLOR NOTES:
 * Banner Dark Blue Background- 050a1d
 * Banner Light Blue grid - 76d4d1
 */

const colors = {
/** ORIGINAL COLORS */

  // transparent: 'transparent',
  // labelColor: '#767676',
  // lightBorder: '#f1f4f6',
  // inactiveField: '#f2f2f2',
  // inactiveButton: '#b7dbdd',
  // inactiveIcon: '#EBEBEB',
  // primaryHover: '#006b70',
  // secondary: '#ff5b60',
  // secondaryHover: '#FF282F',
  // yellow: '#fdb32a',
  // yellowHover: '#F29E02',
  // borderColor: '#dadada',
  // black: '#000000',
  // white: '#ffffff',
  // primary: '#10ac84',
  // headingColor: '#0f2137',
  // quoteText: '#343d48',
  // textColor: 'rgba(52, 61, 72, 0.8)',
  // linkColor: '#2b9eff',

  /**CHANGED COLORS */
  transparent: 'transparent',
  labelColor: '#767676',
  lightBorder: '#f1f4f6',
  inactiveField: '#f2f2f2',
  inactiveButton: '#b7dbdd',
  inactiveIcon: '#EBEBEB',
  primaryHover: '#006b70',
  secondary: '#76d4d1',
  secondaryHover: '#FF282F',
  yellow: '#fdb32a',
  yellowHover: '#F29E02',
  borderColor: '#dadada',
  black: '#000000',
  white: '#ffffff',
  primary: '#050a1d',
  headingColor: '#000000',
  quoteText: '#000000',
  textColor: 'rgba(255, 255, 255, 0.8)',
  linkColor: '#2b9eff',

};

export default colors;
